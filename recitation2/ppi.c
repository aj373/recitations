#include <stdio.h>
int main(int argc, char* argv[]) {
    char playerName[100];
    int height;
    int avgPts;
    float ppi;
    printf("Enter your favourite player's last name: ");
    scanf("%s", playerName);
    printf("Enter player height: ");
    scanf("%d", &height);
    printf("Enter player's points per game: ");
    scanf("%d", &avgPts);
    ppi = (float) avgPts/height;
    printf("%s scored an average of %f points per inch", playerName, ppi);
    return 0;
}