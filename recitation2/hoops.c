#include <stdio.h>

typedef struct public public;
struct HoopsPlayer {
    int playerNumber;
    float ppg;
};

struct HoopsPlayer playerList[10];

void printPlayers(struct HoopsPlayer players[], int numPlayers) {
    for (int i = 0; i < numPlayers; i++) {
        struct HoopsPlayer current = playerList[i];
        printf("Player %d scores %f points per game\n", current.playerNumber, current.ppg);
    }
}

int main(int argc, char* argv[]) {
    int playerNumber;
    float ppg;
    int i;

    for (i = 0; i < 10; i++) {
        printf("Enter player number: ");
        scanf("%d", &playerNumber);

        if (playerNumber == -1) {
            break;
        }
        printf("Enter points per game: ");
        scanf("%f", &ppg);

        playerList[i].playerNumber = playerNumber;
        playerList[i].ppg = ppg;

    }
    printPlayers(playerList, i);
}